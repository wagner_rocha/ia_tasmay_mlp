package tf.ia.mlp;


/**
 * Descreve uma camada de neurônios
 * 
 * @author Sílvia
 * @version 07/11/2013
 */
public class Camada
{
    private Neuronio[] neuronios;
    private int quantidade;
    private int tipoFuncao;
    private int entradas;
    private boolean saida;
    
    /**
     * Cria uma camada de neurônios. A função padrão é a 2 (função logística).
     * @param quantidade corresponde a quantidade de neurônios dessa camada.Se quantidade informada não for válida, a camada será criada com apenas 1 neurônio.
     * @param entradas corresponde a quantidade de entradas de cada neurônio da camada
     */
    public Camada(int quantidade, int entradas){
        this.quantidade = 1;
        if(quantidade>0) this.quantidade = quantidade;
        neuronios = new Neuronio[this.quantidade];
        tipoFuncao = 2;
        if(entradas<=0) this.entradas = 2;
        else this.entradas = entradas;
        alocaNeuronios(entradas);
        saida = false;
    }
    
    /**
     * Cria os neurônios da camada com a quantidade de entradas informada como parâmetro no construtor. 
     */
    private void alocaNeuronios(int entradas){
        for(int i=0; i<quantidade; i++){
            neuronios[i] = new Neuronio(entradas);
        }
    }
    
    /**
     * Devolve true se a camada for de saída
     */
    public boolean isSaida(){
        return saida;
    }
    
    /**
     * Altera o tipo de camada: true é de saída; false é oculta
     */
    public void setTipoCamada(boolean tipo){
        saida = tipo;
    }
    
    /**
     * Devolve a quantidade de neurônios de uma camada
     */
    public int getQuantidade(){
        return quantidade;
    }
    
    /**
     * Devolve o código e o nome da função de transferência usada pelos neurônios da camada.
     * @return código - nome da função de transferência
     */
    public String getTipoFuncao(){
        switch(tipoFuncao){
            case 1: return "1 - Limiar";
            case 2: return "2 - Logística";
        }
        return "3 - Tangente Hiperbólica";
    }
    
    /**
     * Altera o tipo da função de transferência
     * @param tipo deve ser um valor entre 1 e 3. O valor 1 corresponde à função limiar; o 2, à função logística e o 3, à função tangente hiperbólica.
     */
    public void setTipoFuncao(int tipoFuncao){
        if(tipoFuncao>=1 && tipoFuncao<=3) this.tipoFuncao = tipoFuncao;
    }
    
    /**
     * Devolve um neurônio da camada a partir de sua posição
     * @param indice corresponde à posição do neurônio na camada
     * @return devolve o neurônio correspondente. Se o índice não for válido, o método devolverá null.
     */
    public Neuronio getNeuronio(int indice){
        if(indice<0 || indice>= neuronios.length) return null;
        return neuronios[indice];
    }
    
    /**
     * Propaga sinais de entrada pelos neurônios da camada gerando as saídas y. A propagação usará a função determinada pelo atributo tipoFuncao;
     * @param x corresponde ao vetor de entradas do neurônio (pode ser as entradas da rede ou as saídas dos neurônios da camada anterior).
     * @return corresponde ao vetor com as saídas dos neurônios da camada.
     */
    public double[] propaga(double x[]){
        double[] y = new double[quantidade];
        for(int i=0; i<quantidade; i++){
                switch(tipoFuncao){
                    case 1: y[i] = neuronios[i].funcaoLimiar(x); break;
                    case 2: y[i] = neuronios[i].funcaoLogistica(x); break;
                    case 3: y[i] = neuronios[i].funcaoTangenteHiperbolica(x); break;
                }
            }
        return y;
    }
    
   
    /**
     * Calcula gradiente da camada 
     */
    public void calculaGradiente(double erro[]){
        if(neuronios.length!=erro.length) return;
        double derivada = 0;
        for(int i=0; i<quantidade; i++){
                double v = neuronios[i].getV();
               
                switch(tipoFuncao){
                    case 1: derivada = 0; break;
                    case 2: derivada = neuronios[i].derivadaFuncaoLogistica(v); break;
                    case 3: derivada = neuronios[i].derivadaFuncaoTangenteHiperbolica(v); break;
                }
                neuronios[i].setGradiente(derivada * erro[i]);
        }
    }
    

    /**
     * Exibe camada
     */
    public String toString(){
        String msg="";
        if(isSaida()) msg = msg + "(Saida)";
        else msg = msg + "(Oculta)";
        for(int i=0; i<quantidade; i++){
            msg = msg + "Neuronio: " + i +": " ;
            msg = msg + neuronios[i].toString() + "\n";
        }
        return msg;
    }
}
