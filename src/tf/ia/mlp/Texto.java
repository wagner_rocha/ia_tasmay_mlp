/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tf.ia.mlp;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tasmay
 */
public class Texto {
    
    private String arquivoNome; //nome do arquivo no filesystem
    private ClasseTexto classe; //classe do texto 
    private List<String> listaPalavras = new ArrayList<String>(); //lista com as palavras do arquivo
   

    
    public Texto(String arquivoNome, ClasseTexto classe) {
        this.arquivoNome = arquivoNome;
        this.classe = classe;
    }

    public String getArquivoNome() {
        return arquivoNome;
    }

    public void setArquivoNome(String arquivoNome) {
        this.arquivoNome = arquivoNome;
    }

    public ClasseTexto getClasse() {
        return classe;
    }

    public void setClasse(ClasseTexto classe) {
        this.classe = classe;
    }

    public List<String> getListaPalavras() {
        return listaPalavras;
    }

    public void adicionaPalavra(String palavra){
        listaPalavras.add(palavra);
    }
    
}
