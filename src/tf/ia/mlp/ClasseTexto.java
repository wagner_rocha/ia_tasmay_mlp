/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tf.ia.mlp;

/**
 *
 * @author Tasmay
 * 
 * Enum para identificar a classe do texto pelo nome o código
 * 
 */

public enum ClasseTexto {
 
    ANIMAL_DOMESTICO(0,-1,-1),
    CAMP_ELEITORAL(1,-1,1),  
    ENSINO(2,1,-1),
    FEN_NATUREZA(3,1,1);    
    
    private int code;
    private int saida1;
    private int saida2;
    
     
    private ClasseTexto(int code, int saida1, int saida2) {
        this.code = code;
        this.saida1 = saida1;
        this.saida2 = saida2;
    }
     
     

    public int getCode() { return code; }

    public int getSaida1() {
        return saida1;
    }

    public int getSaida2() {
        return saida2;
    }
     
     
  
}
