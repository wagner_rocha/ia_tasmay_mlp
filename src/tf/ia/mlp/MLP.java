package tf.ia.mlp;


/**
 * Classe MultiLayer Perceptron (MLP)
 * 
 * @author Silvia Moraes
 * @version 07/11/2013
 */
public class MLP
{
    private Camada[] camadas;
    private int dimensoes[];
    private int quantidade[];
    private int entrada, numCamadas;
    private double eta;
    private int epocas;
    private double EMQEsperado;
    private double ErroInstantaneo[];
    
    /**
     * Aloca uma rede MLP de n camadas de neurônios. A última camada corresponde à de saídas e as demais (anteriores) são chamadas oculta ou intermediárias.
     * @param dimensoes corresponde a quantidade de neurônios de cada camada. Por exemplo, se as dimensões forem [10,5, 1], criará uma rede MLP 10 x 5 x 1
     * @param entrada corresponde a quantidade de sinais de entrada da rede
     * 
     */
    public MLP(int dimensoes[], int entrada){
        if(entrada<=0) entrada = 2;
        this.entrada = entrada;
        numCamadas = dimensoes.length;
        camadas= new Camada[numCamadas];
        alocaCamadas(dimensoes,entrada);
        this.dimensoes = dimensoes;
        eta = 1;                                //Valores padrões; comentar na dcoumentação
        epocas = 100;
        EMQEsperado = 0.1;
    }
    /** 
     * Aloca a rede
     */
    private void alocaCamadas(int dimensoes[], int entrada){
        quantidade = new int[dimensoes.length];
        quantidade[0] = entrada;
        for(int i=0; i<dimensoes.length; i++){
            //System.out.println("Camada: " + i + " entradas: " + quantidade[i]);
            camadas[i] = new Camada(dimensoes[i],quantidade[i]);
            if(i+1<quantidade.length) quantidade[i+1] = camadas[i].getQuantidade();
        }
        camadas[numCamadas-1].setTipoCamada(true);  //Seta a última camada para camada de saída
    }
    
    /**
     * Devolve o valor da taxa de aprendizagem
     */
    public double getEta(){ return eta; }
    
    /**
     * Altera a taxa de aprendizagem 
     * @param eta deve ser um valor positivo
     */
    public void setEta(double eta){
        if(eta>0) this.eta = eta;
    }
    
    /**
     * Devolve o numero de épocas
     */
    public int getEpocas(){ return epocas; }
    
    /**
     * Altera o número de épocas
     */
    public void setEpocas(int epocas){
        if(epocas>0) this.epocas = epocas;
    }
    
    /**
     * Devolve o erro médio quadrado esperado
     */
    public double getEMQEsperado(){ return EMQEsperado; }
    
    /**
     * Altera o EMQ
     */
    public void setEMQEsperado(double EMQEsperado){
        if(EMQEsperado>0) this.EMQEsperado = EMQEsperado;
    }
    /**
     * Devolve uma camada
     * @param posicao correspodende ao indice da camada 
     */
    public Camada getCamada(int posicao){
        if(posicao<0 || posicao>numCamadas) return null;
        return camadas[posicao];
    }
    
    /**
     * Devolve as dimensoes da rede (topologia)
     */
    public int[] getDimensoes(){
        return dimensoes;
    }
    
    /**
     * Devolve a quantidade de entradas de cada neurônio de cada camada
     */
    public int[] getQuantidade(){
        return quantidade;
    }
    
    public void backpropagation(double x[][], double d[][]){
        ErroInstantaneo = new double[x.length];   //erro instantaneo - um para cada padrão
        
        for(int iter=0; iter<epocas; iter++){
            double EMQ = 0;
            for(int p=0; p<x.length; p++){
               
                //PROPAGAÇÂO
                double padrao[] = new double[x[p].length];     
              //  System.out.print(">>Padrão: " + p + " - [" );
                for(int k=0; k<x[p].length; k++){
                   // System.out.print(x[p][k]+ " , ");
                    padrao[k] = x[p][k];
                }
                //System.out.println("]");
                
                double[] y = propagacao(padrao);
                /*System.out.print("\n>>Saidas geradas: ");
                for(int i=0; i<y.length; i++){
                    System.out.print(y[i] + " ");
                }
                System.out.println("");*/
                
                //RETROPROPAGAÇÂO
                double saidaDesejada [] = new double[d[0].length];
                for(int k=0; k<d[0].length; k++) saidaDesejada [k] = d[p][k];
                
               /* System.out.println(">>>>>>>>>>>");
                System.out.print("Padrão: ");
                for(int k=0; k<padrao.length; k++) System.out.print(padrao[k]+" ");
                System.out.print("\nSaida Desejada: ");
                for(int k=0; k<saidaDesejada.length; k++) System.out.print(saidaDesejada[k]+" ");
                System.out.println("");
                System.out.println(">>>>>>>>>>>");*/
                
                double somaErros = retropropagacao(padrao,y,saidaDesejada);
                ErroInstantaneo[p] = 1.0/2 * somaErros;
                EMQ = EMQ + ErroInstantaneo[p];

            }
            EMQ = EMQ/x.length;
            System.out.println("Epoca: " + iter + " - Erro médio quadrado: " + EMQ);
            if(EMQ<=EMQEsperado) break;
        }
    }
    
    public double[] propagacao(double x[]){
        double[] y = new double[1];
        for(int i=0; i<numCamadas; i++){
            y = camadas[i].propaga(x);
            //System.out.print("Saidas geradas pela camada " + i + ":");
            /*for(int j=0; j<y.length; j++){
                    System.out.print(y[j] + " ");
            }*/
            x = y;
        }
        return y;
    }
    public double retropropagacao(double x[], double y[], double d[]){
         if(y.length!=d.length) return -1;
         double erro[] = new double[y.length];

         double soma = 0;
         for(int i=0; i<y.length; i++){
             erro[i] = d[i] - y[i];
             soma = soma + Math.pow(erro[i],2);
         }
         //-------------------
         //CAMADA DE SAIDA
         //-------------------
         int indiceCamadaSaida = numCamadas-1;                   //indice da camada de saída
         camadas[indiceCamadaSaida].calculaGradiente(erro);      //calcula o gradiente da camada de saida
         
         //Busca entradas
         double saidasCamadaAnterior[] = buscaEntradas(indiceCamadaSaida);   

         //Ajusta pesos
         for(int i=0; i<y.length; i++){
             camadas[indiceCamadaSaida].getNeuronio(i).ajustaPesos(eta,saidasCamadaAnterior);
         }
         
         //-------------------
         //CAMADA OCULTA
         //-------------------
         int camadaAtual = numCamadas-2;
         while(camadaAtual>=0){
             int conexoes = dimensoes[camadaAtual + 1];
             double errosOculta[] = new double[camadas[camadaAtual].getQuantidade()];
             for(int i=0; i<conexoes; i++) errosOculta[i]=0;
             
             for(int n=0; n<camadas[camadaAtual].getQuantidade(); n++)
                for(int k=0; k<conexoes; k++){
                     double w[] = camadas[camadaAtual+1].getNeuronio(k).getPesos();
                     errosOculta[n] += camadas[camadaAtual+1].getNeuronio(k).getGradiente() * w[n+1];
                 }
             camadas[camadaAtual].calculaGradiente(errosOculta);
             
             if(camadaAtual == 0) saidasCamadaAnterior = x;
             else saidasCamadaAnterior = buscaEntradas(camadaAtual);   
             
             //Ajusta pesos
             for(int i=0; i<y.length; i++){
                camadas[camadaAtual].getNeuronio(i).ajustaPesos(eta,saidasCamadaAnterior);
             }
             camadaAtual--;
         }
         //ajusta camadas ocultas
         return soma;
    }
    
    /**
     * Busca as entradas de uma camada da rede
     * @param camadaAtual camada da rede
     */
    
    public double[] buscaEntradas(int camadaAtual){
         int indiceCamadaAnterior = camadaAtual-1;                    //Camada anterior
         if(indiceCamadaAnterior< 0) return null;
         int entradaNeuronio = dimensoes[indiceCamadaAnterior];       //quantidade de neurônios da camada anterior = número de entradas da camada atual
        
         double saidasCamadaAnterior[] = new double[entradaNeuronio];       
         for(int i=0; i<entradaNeuronio; i++){
             saidasCamadaAnterior[i] = camadas[indiceCamadaAnterior].getNeuronio(i).getY();
         }
         return saidasCamadaAnterior;
    }
    
    /**
     * Exibe Topologia
     */
    public String toString(){
        String msg="";
        for(int i=0; i<numCamadas; i++){
            msg = msg + "Camada " + i + "\n";
            msg = msg + camadas[i];
        }
        return msg;
    }
}
