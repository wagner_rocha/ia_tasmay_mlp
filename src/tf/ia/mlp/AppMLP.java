package tf.ia.mlp;


/**
 * Write a description of class MLP here.
 * 
 * @author Sílvia
 * @version 07/11/2013
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
public class AppMLP
{
   private Scanner in;
   private MLP rede;
   private int camadas;         //Numero de camafas
   private int dimensoes[];     //dimensoes: quantidade de neurônios em cada camada
   private int entradas;        //Numero de entradas da rede
   private double x[][];        //Dados de entrada da rede - linhas: quantidade de padrões, coluna: entradas da rede (o padrão)
   private double d[][];        //Saidas desejadas da rede
   //private double e[][];        //Dados completos de entrada 
   //private double s[][];        //Dados completos de saida 
   
   private List<Texto> listaTreino;     //lista contendo 80% dos textos 
   private List<Texto> listaTeste;      //lista contendo 20% dos textos 
   private List<String> bagOfWords;     //lista de palavras bag-of-words
   private int N;                       //N termos com maior frequencia nos textos
  
      /**
    * Método main
    */
   public static void main(String args[]){
       AppMLP mlp= new AppMLP(); 
       mlp.treinamento();
       mlp.generalizacao();
    }

  
   /**
    * Cria a rede 
    */
   private AppMLP(){
        N = 20;
        listaTreino = new ArrayList<Texto>();
        listaTeste = new ArrayList<Texto>();
        bagOfWords = new ArrayList<>();
   
       inicio(); //imprime cabeçalho 
       carregaDados(); //carrega dados dos arquivos texto
       removeCaracteresEspeciais();
       removeStopWords();
       geraBagOfWords();
       defineDadosTreinamento();
        
       defineTopologia(); 
       rede = new MLP(dimensoes,entradas);
       defineConfiguracao();    

   }
   
   private void defineDadosTreinamento() {
       defineDados(listaTreino);
       imprimeDadosLista(listaTreino, "Lista de Treinamento");
   }
   
   private void defineDadosTeste() {
       defineDados(listaTeste);
       imprimeDadosLista(listaTeste, "Lista de Teste");
   }
   
   /**
    * Define os dados a serem usados no treinamento da rede
    * 
    */
    private void defineDados(List<Texto> lista) {
        //tamanha dos dados de entrada
        int entSize = bagOfWords.size();
        x = new double[lista.size()][entSize];  //4 padrões de entrada  80%
        d = new double[lista.size()][2];    //4 saídas desejadas

        //Converte os dados de entrada em binário
        // 0 se a palavra não existir na bag-of-words
        //1 se a palavra existir 
        for (int i = 0; i < lista.size(); i++) {
            Texto tx = lista.get(i);

            for (int j = 0; j < entSize; j++) {
                if (tx.getListaPalavras().contains(bagOfWords.get(j))) {
                    x[i][j] = 1;
                } else {
                    x[i][j] = 0;
                }
            }
            
            //definição das saidas 
            d[i][0] = tx.getClasse().getSaida1();
            d[i][1] = tx.getClasse().getSaida2();
        }
    }
    
   /**
    * Solicita ao usuário a topologia da rede.
    */
   private void defineTopologia(){
       System.out.println("\n>> Definindo a topologia: ");
       in = new Scanner(System.in);
       
       entradas = x[0].length;  
       System.out.println("Entradas da rede: " + entradas);
       
       do{
            System.out.print("Informe a quantidade de camadas (minímo: 2 camadas): ");
            camadas = in.nextInt();
        }while(camadas<2); 
        
       dimensoes = new int[camadas];
       int i;
       for(i=0; i<camadas-1; i++){
            do{
                System.out.print("Informe a quantidade de neurônios da camada oculta " + i + "(minimo: 1 neurônio):");
                dimensoes[i] = in.nextInt();
            }while(dimensoes[i]<1);
       }
       do{
            System.out.print("Informe a quantidade de neurônios da camada de saída " + i + " (minimo: 1 neurônio):");
            dimensoes[i] = in.nextInt();  
       }while(dimensoes[i]<1);
       
       System.out.print("Topologia escolhida: " + entradas);
       for(i=0; i<camadas; i++){
            System.out.print(" x " + dimensoes[i]);
        }
        System.out.println("");
   }
   

   /**
    * Define a configuração da rede
    */
    private void defineConfiguracao(){
           System.out.println("\n>> Definindo a configuração: ");
           double eta;          //Taxa de Aprendizagem
           double EMQ;          //Erro Médio Quadrado esperado
           int epocas;          //Numero máximo de épocas
           int tipoFuncao;      //Define o tipo da função
           
           System.out.print("Informe a taxa de aprendizagem: ");
           eta = in.nextDouble();
           rede.setEta(eta);
           
           System.out.print("Informe o erro médio quadrado esperado: ");
           EMQ = in.nextDouble(); 
           rede.setEMQEsperado(EMQ);
           
           System.out.print("Informe o número máximo de épocas: ");
           epocas = in.nextInt();
           rede.setEpocas(epocas);
           
           for(int i=0; i<camadas; i++){
               System.out.print("Informe o tipo de função para camada " + i + " - use 1 para limiar, 2 para logística e 3 para tangente hiperbólica: ");
               tipoFuncao = in.nextInt();
               rede.getCamada(i).setTipoFuncao(tipoFuncao);
            }
           
    }
   
    /**
     * Inicia treinamento:usa conjunto de treino
     */
    private void treinamento(){ 
       System.out.println("\n--------------------------------------------");
       System.out.println(">> TREINAMENTO");
       System.out.println("--------------------------------------------");
       System.out.println("\n>>Pesos iniciais: ");
       System.out.println(rede);
       rede.backpropagation(x,d);
       System.out.println("\n>>Pesos finais: ");
       System.out.println(rede);
    }

           
    /**
     * Generalização. Com os pesos finais passa pela rede o conjunto de teste.
     */
    private void generalizacao(){
        int acertos = 0;
        System.out.println("\n--------------------------------------------");
        System.out.println(">> GENERALIZACAO");
        System.out.println("--------------------------------------------");
        defineDadosTeste();
        
        for(int i=0; i<x.length; i++){
            System.out.print("\nPadrao: " +i + " - ");
            for(int j=0; j<x[0].length; j++){
               // System.out.print("x[" + i + "," +j +"]=" + (int)x[i][j] + " ");
                System.out.print((int)x[i][j]);
            }
            double y[]=rede.propagacao(x[i]);
            for(int j=0; j<y.length; j++){
                System.out.print(" Saida desejada: " + d[i][0] + " - Saida da rede: " + y[j]);
            }
            y = posProcessamento(y);
            
            for(int j=0; j<y.length; j++){
                System.out.print(" - Saida Processada: " + y[j]);
                if((int)y[j]==(int)d[i][j])  acertos++;
            }  
        }
        acertos = acertos /2;
        int erros = d.length - acertos;
        System.out.println("\n Acertos: " + acertos + " Erros: " + erros);
        calculaMatrizDeConfusao();
    }
    /**
     * Matriz de confusao
     */
    private void calculaMatrizDeConfusao(){
        int matriz[][] = new int[4][4];
        
        for(int i=0; i<4; i++)
          for(int j=0; j<4;j++) matriz[i][j]=0;
       
        System.out.println("\nMatriz de confusao");
        for(int i=0; i<x.length; i++){
            double y[]=rede.propagacao(x[i]);
            y = posProcessamento(y);
            
            if((int)y[0]==-1 && (int)y[1]==-1){  //Setosa
                if((int)d[i][0]==-1 && (int)d[i][1]==-1) matriz[0][0]++;
                if((int)d[i][0]==-1 && (int)d[i][1]==1) matriz[0][1]++;
                if((int)d[i][0]==1 && (int)d[i][1]==-1) matriz[0][2]++;
                if((int)d[i][0]==1 && (int)d[i][1]==1) matriz[0][3]++;
            }
            else if((int)y[0]==-1 && (int)y[1]==1){  //Versicolor
                if((int)d[i][0]==-1 && (int)d[i][1]==-1) matriz[1][0]++;
                if((int)d[i][0]==-1 && (int)d[i][1]==1) matriz[1][1]++;
                if((int)d[i][0]==1 && (int)d[i][1]==-1) matriz[1][2]++;
                if((int)d[i][0]==-1 && (int)d[i][1]==1) matriz[1][3]++;
            }
            else if((int)y[0]==1 && (int)y[1]==-1){  //Setosa
                if((int)d[i][0]==-1 && (int)d[i][1]==-1) matriz[2][0]++;
                if((int)d[i][0]==-1 && (int)d[i][1]==1) matriz[2][1]++;
                if((int)d[i][0]==1 && (int)d[i][1]==-1) matriz[2][2]++;
                if((int)d[i][0]==-1 && (int)d[i][1]==1) matriz[2][3]++;
            }
            else if((int)y[0]==1 && (int)y[1]==1){  //Setosa
                if((int)d[i][0]==-1 && (int)d[i][1]==-1) matriz[3][0]++;
                if((int)d[i][0]==-1 && (int)d[i][1]==1) matriz[3][1]++;
                if((int)d[i][0]==1 && (int)d[i][1]==-1) matriz[3][2]++;
                if((int)d[i][0]==-1 && (int)d[i][1]==1) matriz[3][3]++;
            }
            
        }
        System.out.println("\t\t\t ANIMAL_DOMESTICO     \t CAMP_ELEITORAL \t ENSINO  \t FEN_NATUREZA      ");
        for(int i=0; i<4; i++){
           switch(i){
               case 0: System.out.print("ANIMAL_DOMESTICO     "); break;
               case 1: System.out.print("CAMP_ELEITORAL       "); break;
               case 2: System.out.print("ENSINO               "); break;
               case 3: System.out.print("FEN_NATUREZA         "); break;
           }
           for(int j=0; j<4; j++){
               System.out.print("\t\t\t"+matriz[i][j]);
            }
            System.out.println("\n");
        }
        
        Avaliacao metricas = new Avaliacao(matriz); 
        String[] rotulos = new String[4];
        rotulos[0] = "ANIMAL_DOMESTICO"; rotulos[1]="CAMP_ELEITORAL"; rotulos[2]= "ENSINO"; rotulos[3] = "FEN_NATUREZA";
        metricas.desativa(3);   //usado para desprezar a classe "outra" (que não é válida) no cálculo das médias das métricas de avaliação
        metricas.setRotulos(rotulos);
        System.out.println(metricas);
    }
    
    /**
     * Converte as saidas da rede
     */
    private double[] posProcessamento(double y[]){
        for(int i=0; i<y.length; i++){
            if(y[i]>=0.8) y[i] = 1;
            else y[i] = -1;
        }
        return y;
    }
   

      /**
     * Carrega aruivos de texto da pasta ASSUNTOS do projeto
     * separa 80% dos arquivos de cada categoria e adiciona na lista e treino 
     * separa 20% dos arquivos de cada categoria e adiciona na lista e teste
     */
    private void carregaDados() {

        System.out.println(">> Carregando textos para lista...");

        for (ClasseTexto classe : ClasseTexto.values()) {

            System.out.println(">> Carregando classe " + classe);

            File dir = new File("ASSUNTOS/" + classe.name());
            
            int contArq = dir.listFiles().length;
            
            int treinoCont = (int)(contArq * 0.8);
            
            int i = 0;
            for (File file : dir.listFiles()) {

                Texto tx = new Texto(file.getName(), classe);
                
                try {
                    String linha;
                    BufferedReader leitor = new BufferedReader(new FileReader(file));

                    while ((linha = leitor.readLine()) != null) {
                        tx.adicionaPalavra(linha);
                    }

                } catch (IOException ex) {
                    Logger.getLogger(AppMLP.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                if(i <= treinoCont)
                    listaTreino.add(tx);
                else
                    listaTeste.add(tx);
                i++;
            }
        }
        

        System.out.println(">> Fim da carga de textos");

    }
    
      /**
     * Remove caraateres especiais da lista de TREINO
     */
    private void removeCaracteresEspeciais(){
        
        List<String> invalidos = Arrays.asList(
                ".",",","'","+", "-", "&", "|", "!", "(", ")", "{", "}", "[", "]", "^",
                "~", "*", "?", ":",";","\"","\\","/","%", "#", "@", " ", "1","2","3","4","5","6","7","8","9","0");
        
        for (Texto t: listaTreino) {
                t.getListaPalavras().removeAll(invalidos);
        }
    }
    
    /**
     * Remove StopWords da lista de TREINO
     */
    private void removeStopWords() {

        List<String> stoplist = new ArrayList<>();

        File file = new File("ASSUNTOS/stoplist.txt");

        try {
            String linha;
            BufferedReader leitor = new BufferedReader(new FileReader(file));

            while ((linha = leitor.readLine()) != null) {
                stoplist.add(linha);
            }
        } catch (IOException ex) {
            Logger.getLogger(AppMLP.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Texto t : listaTreino) {
                t.getListaPalavras().removeAll(stoplist);
        }
    }
    
    /**
     * Gera lista de BagOfWords
     */
    private void geraBagOfWords() {

        // cria um hashmap para contar cada palavra (palavra, frequencia)
        HashMap<String, Integer> animalDomesticoCount = new HashMap<>();
        HashMap<String, Integer> campEleitoralCount = new HashMap<>();
        HashMap<String, Integer> ensinoCount = new HashMap<>();
        HashMap<String, Integer> fenNaturezaCount = new HashMap<>();

        for (Texto t : listaTreino) {

                for (String palavra : t.getListaPalavras()) {
                    //se a palavra não existir no MAP adiciona com o valor 1
                    //se a palavra existir incrementa a contagem
                    switch (t.getClasse()) {
                        case ANIMAL_DOMESTICO:
                            Integer count = animalDomesticoCount.get(palavra);
                            animalDomesticoCount.put(palavra, (count == null) ? 1 : count + 1);
                            break;
                        case CAMP_ELEITORAL:
                            Integer count2 = campEleitoralCount.get(palavra);
                            campEleitoralCount.put(palavra, (count2 == null) ? 1 : count2 + 1);
                            break;
                        case ENSINO:
                            Integer count3 = ensinoCount.get(palavra);
                            ensinoCount.put(palavra, (count3 == null) ? 1 : count3 + 1);
                            break;
                        case FEN_NATUREZA:
                            Integer count4 = fenNaturezaCount.get(palavra);
                            fenNaturezaCount.put(palavra, (count4 == null) ? 1 : count4 + 1);
                            break;
                        default:
                    }
                }
        }
        
        //Ordena de forma decrescente em um treemap
        TreeMap<String, Integer> animalDomesticoSort = ordenaHashMap(animalDomesticoCount);
        TreeMap<String, Integer> campEleitoralSort = ordenaHashMap(campEleitoralCount);
        TreeMap<String, Integer> ensinoSort = ordenaHashMap(ensinoCount);
        TreeMap<String, Integer> fenNaturezaSort = ordenaHashMap(fenNaturezaCount);
        
        //concatena as 4 listas 
        concatenaBagOfWords(animalDomesticoSort);
        concatenaBagOfWords(campEleitoralSort);
        concatenaBagOfWords(ensinoSort);
        concatenaBagOfWords(fenNaturezaSort);
    
        System.out.println(">> bag-of-words: ");
        
        for (String s : bagOfWords) {
            System.out.println(s);
        }
        
    }
    
    
    private void imprimeDadosLista(List<Texto> lista, String nome){
        
        System.out.println("+-------------------------------------------------------+");
        System.out.println("|\t\t" + nome);
        System.out.println("+-------------------------------------------------------+");
        
        for (int i = 0; i < x.length; i++) {
            String classe  = lista.get(i).getClasse().name();
            String entrada = "";
            for (int j = 0; j < x[i].length; j++) {
                entrada += (int)x[i][j];    
            }
            
            String saida = d[i][0] + ", " +d[i][1];
            
            System.out.printf("Classe: %-18s  %-10s Saida: %-20s\n", classe, entrada, saida);        
        }
    }
    
    /**
     *  Seleciona as N palavras coma maior frequencia da lista ordenada
     * Se a apalvra já existir não adiciona
     *
     */    
    private void concatenaBagOfWords(TreeMap<String, Integer> treeMap){
        int i = 0;                
        for(Map.Entry<String,Integer> entry : treeMap.entrySet()) {
            String key = entry.getKey();
             i++;
            if (!bagOfWords.contains(key)){
                bagOfWords.add(key);
            }
            if (i == N)
                return;
        }      
    }
    
    
    /**
     *  Função pra ordenar o treemap
     *
     */ 
    private TreeMap<String,Integer> ordenaHashMap(HashMap<String, Integer> map){
        ValueComparator bvc =  new ValueComparator(map);
        TreeMap<String,Integer> sorted_map = new TreeMap<>(bvc);
        sorted_map.putAll(map);
        return sorted_map;
    }
    
    /**
     *  Comparador do  treemap para ordenação
     *
     */ 
    private class ValueComparator implements Comparator<String> {

        Map<String, Integer> base;

        public ValueComparator(Map<String, Integer> base) {
            this.base = base;
        }

        @Override
        public int compare(String a, String b) {
            if (base.get(a) >= base.get(b)) {
                return -1;
            } else {
                return 1;
            } 
        }
    }
    
    private void inicio(){
        System.out.println("+-------------------------------------------------------+");
        System.out.println("|         Trabalho II: Redes Neurais                   |");
        System.out.println("+-------------------------------------------------------+");
        System.out.println(">> Iniciando rede MLP..."); 
    }
    
}

    

