package tf.ia.mlp;


/**
 * Write a description of class Avaliacao here.
 * 
 * @author Silvia 
 * @version 13/11/2013
 */
public class Avaliacao
{
    private int[][] matriz;
    private double[] precision, recall, f1;
    private String[] rotulos;
    private boolean[] ativas;
   
    /**
     * Cria um objeto avaliação
     * @param matriz é a matriz de confusao
     */
    public Avaliacao(int[][] matriz){
        this.matriz = matriz;
        int tam = matriz.length;
        if(tam==0) tam = 2;
        precision = new double[tam];
        recall = new double[tam];
        f1 = new double[tam];
        rotulos = new String[tam];
        ativas = new boolean[tam];
        inicializacao();
    }
    
    /**
     * Inicializa vetores
     */
    private void inicializacao(){
        for(int i=0; i<precision.length; i++){
            precision[i] = 0;
            recall[i] = 0;
            f1[i] = 0;
            ativas[i] = true;
        }
    }
    /**
     * Rótulos das classes
     */
    public void setRotulos(String[] rotulos){
        this.rotulos = rotulos;
    }
    
    /**
     * Define rotulos de classe Padrão
     */
    public void defineRotulosPadroes(){
            for(int i=0; i<matriz.length; i++){
                if(rotulos[i]==null) rotulos[i] = "Classe " + i;
            }
    }
    /**
     * Calcula a métrica precision a partir da matriz de confusão
     */
    public void calculaPrecision(){
        
        for(int i=0; i<matriz.length; i++){
            double FP = 0, TP=0;
            for(int j=0; j<matriz[0].length; j++){
                if(i==j) TP = matriz[i][j];
                else FP = FP + matriz[i][j];
            }
            if((TP+FP)!=0) precision[i]= TP/(TP + FP);
        }
    }
    
    /**
     * Calcula a métrica recall a partir da matriz de confusão
     */
    public void calculaRecall(){
        
        for(int j=0;j<matriz.length; j++){
            double TP=0, FN=0;
            for(int i=0; i<matriz.length; i++){
                if(i==j) TP = matriz[i][j];
                else FN = FN + matriz[i][j];
            }
            if((TP+FN)!=0) recall[j]= TP / (TP+FN);
        }
    }
    
    /**
     * Devolve a precisão por categoria
     */
    public double[] getPrecision(){
        calculaPrecision();
        return precision;
    }
    
    /**
     * Devolve a precisão por categoria
     */
    public double[] getRecall(){
        calculaRecall();
        return recall;
    } 
    
    /**
     * Calcula metrica F1
     */
    public void calculaF1(){
        if(precision.length==0 || recall.length==0) return;
        for(int i=0; i<precision.length; i++){
            if((precision[i] + recall[i])!=0) f1[i] = (2 * precision[i] * recall[i])/(precision[i] + recall[i]);
        }
    }
    
    /**
     * Devolve a métrica F1 (média harmônica das medidas Precision e Recall)
     */
    public double[] getF1(){
        calculaF1();
        return f1;
    }
    
    /**
     * Despreza uma classe no cálculo de média
     * @param classe indice da classe a desprezar na média
     */
    public void desativa(int classe){
        if(classe>0 && classe<matriz.length) ativas[classe]=false;
    }
    /**
     * Devolve media de Precision
     */
    public double getMediaPrecision(){
        if(precision.length==0) return 0;
        double soma = 0;
        for(int i=0; i<precision.length; i++) if(ativas[i]) soma = soma + precision[i];
        return soma / getQuantidadeAtivas();
    }
    /**
     * Conta classes ativas
     */
    public int getQuantidadeAtivas(){
        int cont = 0;
        for(int i=0; i<ativas.length; i++) if(ativas[i]) cont++;
        return cont;
    }
    /**
     * Devolve media de Recall
     */
    public double getMediaRecall(){
        if(recall.length==0) return 0;
        double soma = 0;
        for(int i=0; i<recall.length; i++) if(ativas[i]) soma = soma + recall[i];
        return soma / getQuantidadeAtivas();
    }
    /**
     * Devolve media de F1
     */
    public double getMediaF1(){
        if(f1.length==0) return 0;
        double soma = 0;
        for(int i=0; i<f1.length; i++) if(ativas[i]) soma = soma + f1[i];
        return soma / getQuantidadeAtivas();
    }
    /**
     * Devolve uma String com as medidas de avaliação
     */
    public String toString(){
        defineRotulosPadroes();
        calculaPrecision();
        calculaRecall();
        calculaF1();
        
        String msg="";
        for(int i=0; i<matriz.length; i++){
            msg = msg + rotulos[i] + " - Precision: " + precision[i] + " Recall: " + recall[i] + " F1: " + f1[i] + "\n";
        }
        msg = msg + "Precision: " + getMediaPrecision() + "\n";
        msg = msg + "Recall: " + getMediaRecall() + "\n";
        msg = msg + "F1: " + getMediaF1() + "\n";
        return msg;
    }
}
