package tf.ia.mlp;


/**
 * Descrição da classe Neurônio.
 * 
 * @author Silvia Moraes
 * @version 07/11/2013
 */
import java.util.Random;
public class Neuronio
{
    //Neuronio para n entradas 
    private double[] w;     //pesos
    private int entradas;   //quantidade de entradas
    private double v;       //campo local induzido
    private double y;       //saida do neurônio
    private double gradiente;
    
    /**
     * Cria um neurônio para a rede MLP. Os neurônios são inicializados com pesos aleatórios entre -1 e 1.
     * @param entradas corresponde ao número de entradas (N) do neurônio: x1..xN.
     */
    public Neuronio(int entradas){
        this.entradas = 2;
        if(entradas>0) this.entradas = entradas;
        w= new double[this.entradas+1];   //peso extra para o bias
        inicializa();
    }
    
    /**
     * Inicializa os pesos com valores aleatórios entre -1 e 1
     */
    private void inicializa(){
        Random gera = new Random();
        double peso;
        for(int i=0; i<w.length; i++){
            peso = gera.nextDouble();
            if(gera.nextBoolean()) peso = -peso;
            w[i] = peso;
        }
    }
   
    /**
     * Devolve os pesos do neurônio
     */
    public double[] getPesos(){
        return w;
    }
    /**
     * Devolve a quantidade de entradas de um neurônio.
     */
    public int getEntradas(){
        return entradas;
    }
    
    /**
     * Devolve os pesos de um neurônio. Cada neurônio tem um peso extra (bias) que corresponde ao X0. X0 é uma entrada extra que corresponde sempre a 1.
     * @param posicao corresponde aos indices dos pesos. 0 é peso do bias (entrada extra). De 1 a entradas corresponde aos pesos conectados respectivamente às N entradas: x1 a XN.
     * @return devolve da posicao informada como parâmetro. Se a posição não estiver correta, o método devolverá o peso como 0 (zero).
     */
    public double getW(int posicao){ 
        if(posicao>=0 && posicao<w.length) return w[posicao]; 
        return 0;
    }
    
    /**
     * Devolve o valor do campo local induzido
     */
    public double getV(){
        return v;
        
    }
    
    /**
     * Calcula o campo local induzido (v): somatório das entradas ponderadas pelos pesos.
     * @param x vetor com as entradas. O comprimento do vetor x deve ser igual ao número de entradas.
     * @return devolve o valor de v
     */
    public double calculaV(double x[]){ //calcula o campo local induzido
        if(x.length != entradas) return 0;
        double soma = w[0];
        for(int i=1; i<w.length; i++){
            soma = soma + w[i] * x[i-1];
        }
        v = soma;
        return soma;
    } 
    
    /**
     * Ajusta pesos dos neurônio
     * @param eta constante de aprendizagen
     * @param x entrada do neurônio (se for a 1 camada x é a própria entrada; cc é a saída da camada anterior)
     */
    public boolean ajustaPesos(double eta, double x[]){
        if(x.length != entradas) return false;
        w[0] = w[0] + gradiente * eta;
        for(int i=1; i<w.length; i++){
            w[i]= w[i] + gradiente * eta * x[i-1];
        }
        
        return true;
    }
    /**
     * Aplica a função de transferência limiar (discreta) ao campo local induzido do neurônio.
     * @param x vetor com as entradas. O comprimento do vetor x deve ser igual ao número de entradas.
     * @return devolve o valor de y (saida do neurônio). O valor de y será 0 ou 1.
     */
    
    public double funcaoLimiar(double x[]){ 
        double v = calculaV(x);
        
        if(v>=0) y = 1;
        else y = 0;
        return y;
    }

    /**
     * Aplica a função de transferência logística (contínua) ao campo local induzido do neurônio.
     * @param x vetor com as entradas. O comprimento do vetor x deve ser igual ao número de entradas.
     * @return devolve o valor de y (saida do neurônio). O valor de y estará entre 0 e 1.
     */    
    public double funcaoLogistica(double x[]){
        double v = calculaV(x);
        
        return y = 1.0/(1+Math.exp(-v));
    }

    /**
     * Aplica a função de transferência tangente hipebólica (contínua) ao campo local induzido do neurônio.
     * @param x vetor com as entradas. O comprimento do vetor x deve ser igual ao número de entradas.
     * @return devolve o valor de y (saida do neurônio). O valor de y estará entre -1 e 1.
     */      
    public double funcaoTangenteHiperbolica(double x[]){ 
        double v = calculaV(x);
        
        return y = Math.tanh(v);
    }
    
   /**
    * Devolve o sinal de saída do neuronio
    */
   public double getY(){
       return y;
    }
    /**
     * Atualiza os pesos do neurônio
     * @param posicao corresponde aos indices dos pesos. 0 é peso do bias (entrada extra). De 1 a entradas corresponde aos pesos conectados respectivamente às N entradas: x1 a XN.
     */
    public void setW(int posicao, double novoValor){
        if(posicao>=0 && posicao<w.length) w[posicao] = novoValor; 
    }
    
    /**
     * Gera um string com os pesos dos neurônios.
     */
    
    public String toString(){ 
        String msg="";
        for(int i=0; i<w.length; i++){
            msg = msg + "w["+ i + "]=" + w[i] + " ";
        }
        return msg;
    }
    
    /**
     * Derivada da função logistica
     */
    public double derivadaFuncaoLogistica(double v){
        double s =  1.0/(1+Math.exp(-v));
        return s * (1-s);
    }
    
    /**
     * Derivada da função tangente Hiperbólica
     */
    public double derivadaFuncaoTangenteHiperbolica(double v){
        double s = Math.tanh(v);
        return 1 - (s*s);
    } 
    
    /**
     * Devolve o gradiente do neurônio
     */
    public double getGradiente(){
        return gradiente;
    }
    /**
     * Altera o gradiente do neurônio
     */
    public void setGradiente(double gradiente){
        this.gradiente = gradiente;
    }
}
